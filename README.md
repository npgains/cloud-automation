# Core Engine #

Core Engine is a suite of administrative tools that automate repetitive DevOps tasks routinely encountered by NP Gains, LLC in launching new products.

# Cloud Automation #

This repository launches and configures a cloud infrastructure that's immediately scalable and secure.

### Version: ###

0.0.1

### Company: ###

For more information about NP Gains [visit npgains.com](https://npgains.com)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines: ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

Jake Kim (Owner): jake@npgains.com

*This repository is developed, owned and maintained by the NP Gains and is intended for authorized Nopain, LLC use only.*